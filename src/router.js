import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import WithoutMarker from './views/WithoutMarker.vue'
import MultiMarker from './views/MultiMarker.vue'
import Test from './views/Test.vue'
import Campfire from './views/Campfire.vue'
Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Test
    },
    {
      path: '/multimarker',
      name: 'multimarker',
      component: MultiMarker
    },
    {
      path: '/withoutmarker',
      name: 'withoutmarker',
      component: WithoutMarker
    },
    {
      path: '/test',
      name: 'test',
      component: Home
    },
    { 
      path: '/campfire',
      name: 'campfire',
      component: Campfire
    }
  ]
})
